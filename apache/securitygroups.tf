resource "aws_security_group" "apache_group" {
  name        = "apache_group"
  description = "Allows ssh and port http"
  vpc_id      = "${var.vpc_id}"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["${var.office_ip}", "${var.automation_server_ip}"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["${var.office_ip}", "${var.automation_server_ip}"]
    # cidr_blocks = ["${var.world_cidr}"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name    = "${var.prefix}_group",
    Environment = "${var.prefix}"
  }
}
