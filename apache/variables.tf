# variable "access_key" {}
# variable "secret_key" {}
variable "region" {
  # default = "us-east-1"
}

variable "key_name" {
  # default = "TommyNV"
}

variable "image_id" {
  # default = "ami-009d6802948d06e52"
}

variable "vpc_id" {
  # default = "vpc-eb563690"
}

variable "SubnetId" {
  # default = "subnet-01bee13857f2290de"
}

variable "prefix" {
  default = "vespene"
}

variable "security_group" {
  # default = "vespene"
}

variable "office_ip" {
  default = "77.108.144.180/32"
}

variable "world_cidr" {
  default = "0.0.0.0/0"
}

variable "automation_server_ip" {
  # default = "35.174.204.126/32"
}

# variable "amis" {
#   type = "map"
#   default = {
#     "eu-west-2" = "ami-0274e11dced17bb5b"
#     "us-east-1" = "ami-009d6802948d06e52"
#   }
# }
