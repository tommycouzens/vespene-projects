terraform {
  backend "s3" {
    bucket = "vespenebucket"
    key    = "apache/terraform.tfstate"
    region = "eu-west-2"
  }
}

data "terraform_remote_state" "ec2" {
  backend = "s3"
  config {
    bucket = "vespenebucket"
    key    = "apache/terraform.tfstate"
    region = "eu-west-2"
  }
}
